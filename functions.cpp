#include <iostream>
#include "functions.h"

using namespace std;

//////////////////////////////////////////
/*
 * Fonction permetant d'initialiser la liste circulaire,
 * c'est a dire en renvoyant un pointeur de Maillon
 * initialise a nullptr
 *
 * ENTREE : RIEN
 * SORTIE : Pointeur de Maillon
 */
//////////////////////////////////////////
Maillon *initialisationListeCirculaire() {
    return nullptr;
}


//////////////////////////////////////////
/*
 * Fonction permetant d'afficher la liste circulaire
 * a parir du debut de la liste
 *
 * ENTREE : Pointeur de Maillon
 * SORTIE : RIEN
 */
//////////////////////////////////////////
void afficherListe(Maillon *debutListe) {

    if (debutListe != nullptr) {

        //Declaration et initialisation d'un pointeur de Maillon
        //vers le premier Maillon de la liste
        Maillon *mailonN = debutListe;

        //Premier passage le premier Maillon n'est pas encore affiche
        bool premierMaillonAfffiche = false;

        //Tant que le Maillon n'est pas celui du debut de de liste
        // ou que le Maillon du debut n'as pas ete affiche
        while (mailonN != debutListe || !premierMaillonAfffiche) {

            //Affichage du Maillon actuel
            cout << mailonN->numero;

            //Mise en forme si ce n'est pas le dernier mailllon a afficher
            if (mailonN->suivant != debutListe) {
                cout << " - ";
            }

            //Passage au Maillon suivant
            mailonN = mailonN->suivant;

            //Premier Maillon maintenant afficher
            if (!premierMaillonAfffiche) {
                premierMaillonAfffiche = true;
            }
        }
    } else {
        cout << "Huum... La liste est vide, essayez d'inserer un maillon d'abord" << endl;
    }
}

//////////////////////////////////////////
/*
 * Fonction permetant de faire pointer le debut de la liste
 * vers le Maillon suivant le debut de la liste actuel
 *
 * ENTREE : Pointeur de Maillon
 * SORTIE : RIEN
 */
//////////////////////////////////////////
void rotationUnCran(Maillon *&debutListe) {
    if (debutListe != nullptr) {
        debutListe = debutListe->suivant;
    } else {
        cout << "Huum... La liste est vide, essayez d'inserer un maillon d'abord" << endl;
    }
}


//////////////////////////////////////////
/*
 * Fonction permetant d'inserer un nouveau Maillon,
 * ayant le numero passe en parametre, avant le
 * debut de la liste.
 *
 * ENTREE : Entier, Pointeur de Maillon
 * SORTIE : RIEN
 */
//////////////////////////////////////////
void insererMaillon(int numero, Maillon *&debutListe) {

    //Declaration et initialisation d'un nouveau Maillon
    Maillon *nouveauMaillon = (Maillon *) malloc(sizeof(Maillon));

    //Affectation du numero au nouveau Maillon
    nouveauMaillon->numero = numero;

    //Si aucuns Maillon dans la liste
    if (debutListe == nullptr) {
        //Le Maillon suivant est le Maillon lui meme
        nouveauMaillon->suivant = nouveauMaillon;

        //Le debut de la liste devient le nouveau Maillon
        debutListe = nouveauMaillon;
    } else {

        //Declaration et initialisation d'un pointeur vers Maillon,
        //pointant le debut de la liste
        Maillon *mailonN = debutListe;

        //Tant que le Maillon suivant le Maillon actuel n'est pas le
        // debut de la liste (apres un tour complet)
        while (mailonN->suivant != debutListe) {
            //Le Maillon actuel devient le Maillon suivant
            mailonN = mailonN->suivant;
        }

        //Le Maillon actuel prend comme suivant le nouveau Maillon
        mailonN->suivant = nouveauMaillon;

        //Le nouveau Maillon prend comme suivant le debut de la liste
        nouveauMaillon->suivant = debutListe;
    }
}


//////////////////////////////////////////
/*
 * Fonction comptant récursivement le nombre de maillon
 * présent dans la liste circulaire et retournant ce nombre
 *
 * ENTREE : Pointeur de Maillon
 * SORTIE : Entier
 */
//////////////////////////////////////////
int getNbMaillonListe(Maillon *debutListe) {
    //Déclarer et initialiser statiquement le nombre de maillon a 0
    static int NbMaillonListe = 0;

    //Déclarer et initialiser statiquement le comme quoi le premier
    // maillon n'a pas encore ete affiche
    static bool premierMaillonAfffiche = false;

    //Déclarer et initialiser statiquement un pointeur vers le debut de la liste
    static Maillon *toutDebutListe = debutListe;

    //Si le Maillon n'est pas celui du debut de de liste
    // ou que le Maillon du debut n'as pas ete affiche
    if (debutListe != toutDebutListe || !premierMaillonAfffiche) {

        //Incrementation du nombre de maillon
        NbMaillonListe++;

        //Le premier maillon est maintenant affiche
        if (!premierMaillonAfffiche) {
            premierMaillonAfffiche = true;
        }

        //Appel de la fonction recursivement
        getNbMaillonListe(debutListe->suivant);

    }

    //Retourne le nombre de maillons
    return NbMaillonListe;
}

//////////////////////////////////////////
/*
 * Fonction affichant le nombre de maillons present dans la liste circulaire
 *
 * ENTREE : Pointeur de Maillon
 * SORTIE : Entier
 */
//////////////////////////////////////////
void afficherNbMaillonListe(Maillon *debutListe) {
    if (debutListe != nullptr) {
        cout << endl << "La liste comporte : " << getNbMaillonListe(debutListe) << " maillons" << endl;
    } else {
        cout << "Huum... La liste est vide, essayez d'inserer un maillon d'abord" << endl;
    }
}

//////////////////////////////////////////
/*
 * Fonction affichant le menu
 *
 * ENTREE : RIEN
 * SORTIE : RIEN
 */
//////////////////////////////////////////
void affichageMenu() {
    cout
            << endl
            << "----------- MENU -----------"
            << endl
            << "1.Inserer un nouveau maillon"
            << endl
            << "2.Faire une rotation d'un cran"
            << endl
            << "3.Afficher le nombre de maillons"
            << endl
            << "4.Afficher la liste circulaire"
            << endl
            << "5.Quitter"
            << endl
            << "Choix :";
}