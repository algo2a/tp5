#ifndef TP5_FUNCTIONS_H
#define TP5_FUNCTIONS_H

//Declaration de la structure Maillon
struct Maillon {
    int numero;
    Maillon *suivant;
};

//Déclaration des fonctions utilitaires sur la liste circulaire
Maillon *initialisationListeCirculaire();

int getNbMaillonListe(Maillon *debutListe);

void affichageMenu();

//Déclaration des fonctions principale sur la liste circulaire
void afficherListe(Maillon *debutListe);

void rotationUnCran(Maillon *&debutListe);

void insererMaillon(int numero, Maillon *&debutListe);

void afficherNbMaillonListe(Maillon *debutListe);

#endif //TP5_FUNCTIONS_H
