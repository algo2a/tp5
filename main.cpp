#include <iostream>
#include "functions.h"

using namespace std;

int main() {

    int choix = 0;

    //Declaration et initialisation d'un pointeur de Maillon
    //permetant l'identificationdu debut de la liste circulaire
    Maillon *debutListe = initialisationListeCirculaire();

    while (choix != 5) {
        affichageMenu();
        cin >> choix;

        switch (choix) {
            case 1:
                int numero;
                cout << "Numero du maillon que vous voulez inserer : ";
                cin >> numero;

                //Insertion d'un nouveau maillon
                insererMaillon(numero, debutListe);
                break;

            case 2:
                //Faire avancer le pointeur du debut de liste
                rotationUnCran(debutListe);
                break;

            case 3:
                //Affichage du nombre de maillon
                afficherNbMaillonListe(debutListe);
                break;

            case 4:
                //Affichage de la liste
                afficherListe(debutListe);
                break;

            case 5:
                cout << "Bye Bye";
                break;

            default:
                cout << "Vous devriez essayer avec un entier entre 1 et 5 !";
                break;
        }
    }
    return 0;
}

